#pragma once

#ifdef QUORIDORDLL_EXPORTS
#define QUORIDORDLL_API __declspec(dllexport)
#else
#define QUORIDORDLL_API __declspec(dllimport)
#endif 

#include <iostream>
#include <string>
#include<vector>
#include<exception>
#include<fstream>

class QUORIDORDLL_API Logger
{
public:
	enum class Level
	{
		Info,
		Warning,
		Error
	};

public:
	Logger(std::ostream& os, Level minimumLevel = Level::Info);

	void log(const char * message, Level level);
	void log(const std::string& message, Level level);

	void setMinimumLogLevel(Level level);

	void Verificare(int a);
	void VerificareOptiune(char b);

private:
	std::ostream& os;
	Level minimumLevel;
};
