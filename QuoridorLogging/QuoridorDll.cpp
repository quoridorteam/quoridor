#include "QuoridorDll.h"

const char* LogLevelToString(Logger::Level level)
{
	switch (level)
	{
	case Logger::Level::Info:
		return "Info";
	case Logger::Level::Warning:
		return "Warning";
	case Logger::Level::Error:
		return "Error";
	default:
		return "";
	}
}

Logger::Logger(std::ostream & os, Logger::Level minimumLevel) :
	os{ os },
	minimumLevel{ minimumLevel }
{
	// Empty
}

void Logger::log(const char * message, Level level)
{
	if (static_cast<int>(level) < static_cast<int>(minimumLevel))
		return;

	os << "[" << LogLevelToString(level) << "] " << message << std::endl;
}

void Logger::log(const std::string & message, Level level)
{
	this->log(message.c_str(), level);
}

void Logger::setMinimumLogLevel(Level level)
{
	this->minimumLevel = level;
}

void Logger::Verificare(int a)
{
	while (a > 9 || a < 1)
	{
		std::cout << " Numar gresit.Introduceti un numar de la 1 la 9" << std::endl;
		std::cin >> a;
		this->log("Numar gresit.", Logger::Level::Error);
	}
}

void Logger::VerificareOptiune(char b)
{
	while (b != 'P' && b != 'W') {
		std::cout << "Optiune invalida.Introduceti P sau W";
		std::cin >> b;
		this->log("Optiunde invalida", Logger::Level::Error);
	}
}