#pragma once
#include <iostream>
//Square
//Methods :
//
//Members:
//pair<int, int> m_position;
//Direction m_right;
//Direction m_left;
//Direction m_up;
//Direction m_down;
//State m_state;



class Square
{
public:

	enum State
	{
		Ocupied,
		Unocupied
	};

public:
	Square();
	Square(const int& positionX, const int& positionY, const State& right, const State& left, 
		const State& up, const State& down);
	Square(const Square& other);

	Square operator=(const Square& other);
	
	// getters 
	int getPositionX();
	int getPositionY();
	State getRight();
	State getLeft();
	State getUp();
	State getDown();

	//setters
	void setPositionX(int positionX);
	void setPositionY(int positionY);
	void setRight(State right);
	void setLeft(State left);
	void setUp(State up);
	void setDown(State down);

private:
	int m_positionX;
	int m_positionY;
	State m_right;
	State m_left;
	State m_up;
	State m_down;
};

