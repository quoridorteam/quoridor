#pragma once
#include "windows.h"
#include <iostream>
#include "Piece.h"

class Console
{
public:
	using Position = std::pair<uint16_t, uint16_t>;
public:
	Console();
	Console(const Position& def_coord);
	Console(const Console& other);
	Console& operator=(const Console& other);

	Console(Console && other);
	Console& operator=(Console&& other);

	void CursorMove(const float& X, const float& Y) const;
	void PlaceCursorToDef() const;
	void RemoveCharAt(const uint16_t& X, const uint16_t& Y);
	void PrintColouredText(const Piece & piece, const uint8_t& color);

	//Getters
	size_t GetHorizDistance() const;
	size_t GetVerticDistance() const;
	size_t GetBoardWidth() const;
	size_t GetBoardHeight() const;

	uint8_t GetDefColor() const;
	uint8_t GetRed() const;
	uint8_t GetGreen() const;

	COORD GetDefCoord()const;

	//Setters
	void SetDefCoord(COORD&& other);

private: 
	static const size_t kHorizDistance = 6;
	static const size_t kVerticDistance = 3;
	static const size_t kBoardWidth = 54;
	static const size_t kBoardHeight = 27;

	static const uint8_t kDefColor = 15;
	static const uint8_t kRed = 12;
	static const uint8_t kGreen = 10;

	COORD m_defCoord;
};

