#include "Board.h"



Board::Board()
{
}


Board::~Board()
{
}

void Board::PrintBoard() const
{
	std::cout << std::endl << "      A     B     C     D     E     F     G     H     I" << std::endl;
	for (int index = 0; index < 9; ++index)
	{
		std::cout << "     ___   ___   ___   ___   ___   ___   ___   ___   ___ " << std::endl;
		std::cout << " " << index + 1 << "  |   | |   | |   | |   | |   | |   | |   | |   | |   |" << std::endl;
		std::cout << "    |___| |___| |___| |___| |___| |___| |___| |___| |___|" << std::endl;
	}
}

size_t Board::GetBoardWidth() const
{
	return kWidth;
}

size_t Board::GetBoardHeight() const
{
	return kHeight;
}

//Square & Board::operator[](const Square & pos)
//{
	// TODO: insert return statement here
//}

