#include "Piece.h"
#include <cassert>


Piece::Piece()
{
}

Piece::Piece(const char name, std::pair<uint8_t, uint8_t> boardPosition, const int & color) :
	m_name(name),
	m_boardPosition(boardPosition),
	m_color(color)
{
	//static_assert(sizeof(*this) == 1, "Size of Piece is not 1");

}


Piece::Piece(const Piece & other):
	m_name(other.m_name),
	m_color(other.m_color),
	m_boardPosition(other.m_boardPosition)
{
	//assert(false);
}

Piece & Piece::operator=(const Piece & other)
{
	new(this) Piece(other);
	return *this;
}

Piece & Piece::operator=(Piece && other)
{
	this->m_name = other.m_name;
	this->m_color = other.m_color;
	this->m_boardPosition = other.m_boardPosition;
	new (&other) Piece;
	return *this;
}

Piece::Piece(Piece && other)
{
	*this = std::move(other);
}

int Piece::GetColor() const
{
	return m_color;
}

char Piece::GetName() const
{
	return m_name;
}

std::pair<uint8_t, uint8_t> Piece::GetPosition() const
{
	return m_boardPosition;
}

void Piece::SetPosition(const std::pair<uint8_t, uint8_t> &auxPosition)
{
	m_boardPosition = auxPosition;
}

std::ostream & operator<<(std::ostream & os, const Piece & piece)
{
	os << piece.m_name;
	return os;
}
