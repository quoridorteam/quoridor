#pragma once
#include "Player.h"
#include "Board.h"
class BoardStateChecker
{
public:
	enum class State
	{
		Win,
		Lose,
		Draw,
		None

	};

public:
	static State Check(Board &B,const Player& player1,const Player& player2);
};

