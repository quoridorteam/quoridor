#pragma once
#include <iostream>
#include <cassert>
class Piece
{
public:
	Piece();
	Piece(const char name, std::pair<uint8_t, uint8_t> boardPosition, const int& color);
	Piece(const Piece&other);
	Piece& operator = (const Piece& other);
	Piece& operator = (Piece&& other);
	Piece(Piece&& other);

	//Getters
	int GetColor() const;
	char GetName() const;
	std::pair<uint8_t, uint8_t> GetPosition() const;

	//Setters
	void SetPosition(const std::pair<uint8_t, uint8_t> &auxPosition);

	friend std::ostream& operator <<(std::ostream& os, const Piece& piece);

private:
	int m_color;
	char m_name;
	std::pair<uint8_t, uint8_t> m_boardPosition;
};

