#pragma once
#include "Square.h"

#include <array>
#include <optional>

class Board
{
public:
	Board();
	~Board();

	void PrintBoard() const;
	//Square & operator[] (const Square& pos);
	size_t GetBoardWidth()const;
	size_t GetBoardHeight()const;

private:
	static const size_t kWidth = 9;
	static const size_t kHeight = 9;
	static const size_t kSize = kWidth * kHeight;

private:
	std::array<Square, kSize> m_components;
};

