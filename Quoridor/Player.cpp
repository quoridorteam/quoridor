#include "Player.h"


Player::Player()
{
}

Player::Player(const Piece& piece, const std::string name) :
	m_piece(piece),
	m_name(name)
{

}

Player& Player::operator=(const Player & other)
{
	m_piece = other.m_piece;
	m_walls = other.m_walls;
	m_name = other.m_name;
	return *this;
}

Player & Player::operator=(Player && other)
{
	m_piece = std::move(other.m_piece);
	m_walls = std::move(other.m_walls);
	m_name = other.m_name;
	new(&other) Player;
	return *this;
}

Player::Player(Player && other)
{
	*this = std::move(other);
}


Player::Player(const Player & other)
{
	*this = other;
}

void Player::MovePiece(Console &C, Player& passivePlayer, std::vector<std::pair<int, int>> occupiedPosition)
{
	const auto&[coordX, coordY] = m_piece.GetPosition();

	size_t kHorizDistance = C.GetHorizDistance();
	size_t kVerticDistance = C.GetVerticDistance();

	int displayX = coordX * kHorizDistance;
	int displayY = coordY * kVerticDistance;
	system("pause>nul");

	if (GetAsyncKeyState(VK_UP) && (coordY - 1)*kVerticDistance >= kVerticDistance) {
		for (int i = 0; i < occupiedPosition.size(); ++i) {
			if ((occupiedPosition[i].first >= (coordX*C.GetHorizDistance() - C.GetHorizDistance() - 1) &&
				occupiedPosition[i].first <= (coordX * C.GetHorizDistance() - 1)) && (coordY == (occupiedPosition[i].second) / C.GetVerticDistance()+1)) {
				throw("Pozitie ocupata.Alegeti alta directie!");
			}
		}
	}
		

	if ((GetAsyncKeyState(VK_UP) && (coordY - 1)*kVerticDistance >= kVerticDistance) &&
		(coordX == passivePlayer.GetPiece().GetPosition().first) && (coordY == passivePlayer.GetPiece().GetPosition().second + 1)) {
		C.RemoveCharAt(coordX, coordY);
		m_piece.SetPosition(std::make_pair(coordX, coordY - 2));
		//throw("Pozitie ocupata.Alegeti alta directie!")
	}

	if (GetAsyncKeyState(VK_UP) && (coordY - 1)*kVerticDistance >= kVerticDistance)
	{
		
		C.RemoveCharAt(coordX, coordY);
		m_piece.SetPosition(std::make_pair(coordX, coordY - 1));
	}


	if (GetAsyncKeyState(VK_DOWN) && (coordY - 1)*kVerticDistance >= kVerticDistance) {
		for (int i = 0; i < occupiedPosition.size(); ++i) {
			if ( (occupiedPosition[i].first>=(coordX*C.GetHorizDistance()-C.GetHorizDistance()-1)&&
				occupiedPosition[i].first <=( coordX * C.GetHorizDistance()-1) )&& (coordY == (occupiedPosition[i].second) / C.GetVerticDistance() )) {
				throw("Pozitie ocupata.Alegeti alta directie!");
			}
		}
	}
	if ((GetAsyncKeyState(VK_DOWN) && (coordY - 1)*kVerticDistance >= kVerticDistance) &&
		(coordX == passivePlayer.GetPiece().GetPosition().first) && (coordY == passivePlayer.GetPiece().GetPosition().second -1)) {
		//std::cout << "pozitie ocupata" << std::endl;
		C.RemoveCharAt(coordX, coordY);
		m_piece.SetPosition(std::make_pair(coordX, coordY + 2));
		//throw("Pozitie ocupata.Alegeti alta directie!");
	}else if (GetAsyncKeyState(VK_DOWN) && (coordY + 1)*kVerticDistance <= C.GetBoardHeight())
	{
		C.RemoveCharAt(coordX, coordY);
		m_piece.SetPosition(std::make_pair(coordX, coordY+1));

	}


	if (GetAsyncKeyState(VK_RIGHT) && (coordY - 1)*kVerticDistance >= kVerticDistance) {
		for (int i = 0; i < occupiedPosition.size(); ++i) {
			if ((occupiedPosition[i].first==coordX*C.GetHorizDistance()+C.GetVerticDistance())&&
				(occupiedPosition[i].second>=coordX*C.GetVerticDistance())&&(occupiedPosition[i].second>=coordX*C.GetVerticDistance()+C.GetVerticDistance())) {
				throw("Pozitie ocupata.Alegeti alta directie!");
			}
		}
	}

	if ((GetAsyncKeyState(VK_RIGHT) && (coordX + 1)*kHorizDistance <= C.GetBoardWidth() &&
		(coordX == passivePlayer.GetPiece().GetPosition().first-1) && (coordY == passivePlayer.GetPiece().GetPosition().second)) ){
		//std::cout << "pozitie ocupata" << std::endl;
		//throw("Pozitie ocupata.Alegeti alta directie!");
		C.RemoveCharAt(coordX, coordY);
		m_piece.SetPosition(std::make_pair(coordX + 2, coordY));
	}
	else if (GetAsyncKeyState(VK_RIGHT) && (coordX + 1)*kHorizDistance <= C.GetBoardWidth())
	{
		C.RemoveCharAt(coordX, coordY);
		m_piece.SetPosition(std::make_pair(coordX+1, coordY));
	}


	if (GetAsyncKeyState(VK_LEFT) && (coordY - 1)*kVerticDistance >= kVerticDistance) {
		for (int i = 0; i < occupiedPosition.size(); ++i) {
			if ((occupiedPosition[i].first == coordX * C.GetHorizDistance() - C.GetVerticDistance()) &&
				(occupiedPosition[i].second >= coordX * C.GetVerticDistance()) && (occupiedPosition[i].second >= coordX * C.GetVerticDistance() + C.GetVerticDistance())) {
				throw("Pozitie ocupata.Alegeti alta directie!");
			}
		}
	}

	if ((GetAsyncKeyState(VK_LEFT) && (coordX + 1)*kHorizDistance <= C.GetBoardWidth() &&
		(coordX == passivePlayer.GetPiece().GetPosition().first + 1) && (coordY == passivePlayer.GetPiece().GetPosition().second)) ){
		
		C.RemoveCharAt(coordX, coordY);
		m_piece.SetPosition(std::make_pair(coordX - 2, coordY));
	}
	else if (GetAsyncKeyState(VK_LEFT) && (coordX - 1)*kHorizDistance >= kHorizDistance)
	{
		C.RemoveCharAt(coordX, coordY);
		m_piece.SetPosition(std::make_pair(coordX - 1, coordY));

	}
	

	C.CursorMove(m_piece.GetPosition().first, m_piece.GetPosition().second);
	C.PrintColouredText(m_piece, m_piece.GetColor());
	C.PlaceCursorToDef();
}



std::pair<int, int> Player::VerticalWallPosition(Console C, char columnPosition, int linePosition)
{
	std::pair<int, int>position;
	position.second = linePosition*C.GetVerticDistance();
	position.first = ((columnPosition - 'A' + 1)*C.GetHorizDistance()) + 3;
	return position;
}
std::pair<int, int> Player::OrizontalWallPosition(Console C, char columnPosition, int linePosition)
{
	std::pair<int, int>position;
	position.second = linePosition * C.GetVerticDistance()-1;
	position.first = ((columnPosition - 'A' + 1)*C.GetHorizDistance()) + 2-C.GetVerticDistance();
	return position;
}


bool Player::verifyWall(std::vector<std::pair<int, int>> occupiedPosition, const Wall&other)
{
	for (int i = 0; i < occupiedPosition.size(); i++) {
		if (other.GetDirection() == Wall::Type::VERTIC && (other.GetCoord().first == occupiedPosition[i].first)
			&& ((other.GetCoord().second>= occupiedPosition[i].first)&&(other.GetCoord().second<= occupiedPosition[i].first + 3)) ) {
			throw("Choose another position");
			return false;
		}
		else if (other.GetDirection() == Wall::Type::HORIZ && (other.GetCoord().second == occupiedPosition[i].second)

			&& ((other.GetCoord().first <= occupiedPosition[i].first + 6)&&(other.GetCoord().first >= occupiedPosition[i].first))) {
			
			throw("Choose another position");
			return false;
		}
		

	}
	return true;
}
void Player::PlaceWall(const Console &C, const Wall::Type direction,const std::pair<int,int>c)
{	

	C.CursorMove((float)c.first / C.GetHorizDistance(), (float)c.second);
	m_walls[m_toPlaceWalls-1].SetCoord(c);
	m_walls[m_toPlaceWalls-1].SetDirection(direction);
	m_walls[m_toPlaceWalls - 1].afisareWall(c);
	m_toPlaceWalls--;
	
	  C.PlaceCursorToDef();
	
}

std::ostream & operator<<(std::ostream & os, const Player & player)
{
	os << player.m_name;
	return os;
}

Piece Player::GetPiece() const
{
	return m_piece;
}

size_t Player::GetSize() const
{
	return kSize;
}

std::string Player::GetName() const
{
	return m_name;
}

int Player::GetToPlaceWalls() const
{
	return m_toPlaceWalls;
}



void Player::SetPiece(Piece && other)
{
	m_piece = std::move(other);
}

void Player::SetName(std::string && other)
{
	m_name = std::move(other);
}

