#include "Square.h"



Square::Square()
{

}

Square::Square(const int& positionX, const int& positionY, const State& right, const State& left,
	const State& up, const State& down) :
	m_positionX(positionX),
	m_positionY(positionY),
	m_right(right),
	m_left(left),
	m_up(up),
	m_down(down)
{

}

Square::Square(const Square & other)
{
	(*this) = other;
}

Square Square::operator=(const Square & other)
{
	this->m_positionX = other.m_positionX;
	this->m_positionY = other.m_positionY;
	this->m_right = other.m_right;
	this->m_left = other.m_left;
	this->m_up = other.m_up;
	this->m_down = other.m_down;
	return (*this);
}

int Square::getPositionX()
{
	return this->m_positionX;
}

int Square::getPositionY()
{
	return this->m_positionY;
}

Square::State Square::getRight()
{
	return this->m_right;
}

Square::State Square::getLeft()
{
	return this->m_left;
}

Square::State Square::getUp()
{
	return this->m_up;
}

Square::State Square::getDown()
{
	return this->m_down;
}

void Square::setPositionX(int positionX)
{
	this->m_positionX = positionX;
}

void Square::setPositionY(int positionY)
{
	this->m_positionY = positionY;
}

void Square::setRight(State right)
{
	this->m_right = right;
}

void Square::setLeft(State left)
{
	this->m_left = left;
}

void Square::setUp(State up)
{
	this->m_up = up;
}

void Square::setDown(State down)
{
	this->m_down = down;
}


