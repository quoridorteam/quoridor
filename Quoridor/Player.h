#pragma once
#include "windows.h"
#include "Piece.h"
#include "Console.h"
#include <array>
#include <string>
#include "Wall.h"
#include <iostream>
#include <chrono>
#include <thread>

class Player
{
public:
	Player();
	Player(const Piece& piece, const std::string name);
	Player(const Player &other);
	Player& operator = (const Player &other);

	Player& operator = (Player&& other);
	Player(Player&& other);

	void MovePiece(Console &C,Player& passivePlayer, std::vector<std::pair<int, int>> occupiedPosition);
	void PlaceWall(const Console &C,const Wall::Type direction, const std::pair<int, int>c);
	std::pair<int, int>VerticalWallPosition(Console C, char columnPosition, int linePosition);
	std::pair<int, int> OrizontalWallPosition(Console C, char columnPosition, int linePosition);
	bool verifyWall(std::vector<std::pair<int,int>> occupiedPosition, const Wall&other);
	friend std::ostream& operator <<(std::ostream& os, const Player& player);

	//Getters
	Piece GetPiece() const;
	size_t GetSize()const;
	std::string GetName()const;
	int GetToPlaceWalls()const;
	//Setters
	void SetPiece(Piece&& other);
	void SetName(std::string &&other);

private: 
	static const size_t kSize = 10;

	Piece m_piece;
	std::array<Wall, kSize> m_walls;
	std::string m_name;
	int m_toPlaceWalls = 10;
};

