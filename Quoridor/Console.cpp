#include "Console.h"

Console::Console()
{

}

Console::Console(const Position& def_coord)
{
	m_defCoord.X = def_coord.first;
	m_defCoord.Y = def_coord.second;
}

Console::Console(const Console & other)
{
	*this = other;
}

Console & Console::operator=(const Console& other)
{
	m_defCoord = other.m_defCoord;
	return *this;
}

Console::Console(Console && other)
{
	*this = std::move(other);
}

Console & Console::operator=(Console && other)
{
	m_defCoord = std::move(other.m_defCoord);
	new(&other) Console;
	return *this;
}

void Console::CursorMove(const float& X, const float& Y) const
{
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), 
		COORD({ static_cast<short>(X* (kHorizDistance)), static_cast<short>(Y* kVerticDistance) }));
}

void Console::PlaceCursorToDef() const
{
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), COORD({ m_defCoord.X, m_defCoord.Y }));
}

void Console::RemoveCharAt(const uint16_t& X, const uint16_t& Y)
{
	CursorMove(X, Y);
	std::cout << ' ';
}

void Console::PrintColouredText(const Piece & piece, const uint8_t& color)
{
	SetConsoleTextAttribute(HANDLE(GetStdHandle(STD_OUTPUT_HANDLE)), color);
	std::cout << piece;
	SetConsoleTextAttribute(HANDLE(GetStdHandle(STD_OUTPUT_HANDLE)), kDefColor);
}

size_t Console::GetHorizDistance() const
{
	return kHorizDistance;
}

size_t Console::GetVerticDistance() const
{
	return kVerticDistance;
}

size_t Console::GetBoardWidth() const
{
	return kBoardWidth;
}

size_t Console::GetBoardHeight() const
{
	return kBoardHeight;
}

uint8_t Console::GetDefColor() const
{
	return kRed;
}

uint8_t Console::GetRed() const
{
	return kRed;
}

uint8_t Console::GetGreen() const
{
	return kGreen;
}

COORD Console::GetDefCoord() const
{
	return m_defCoord;
}

void Console::SetDefCoord(COORD&& other)
{
	m_defCoord = std::move(other);
}
