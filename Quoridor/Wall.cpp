#include "Wall.h"


Wall::Wall()
{
	m_direction = Type::NONE;
}

Wall::Wall(const Type & direction,const std::pair<int,int>position) : m_direction(direction),coord(position)
{
}

Wall::Wall(const std::pair<int, int> position): coord(position)
{
}


Wall::Wall(const Wall & other)
{

	this->m_direction = other.m_direction;

}

Wall::Type Wall::GetDirection() const
{
	return m_direction;
}

std::pair<int, int> Wall::GetCoord() const
{
	return coord;
}



void Wall::SetDirection(const Type& direction)
{
	m_direction = direction;
}

void Wall::SetCoord(const std::pair<int, int> newCoord)
{
	coord = newCoord;
}

std::ostream& operator<<(std::ostream& os, const Wall & other)
{
	COORD C = { 15, 3 };
	if (other.m_direction == Wall::Type::HORIZ)
		os << "#########";

	else if (other.m_direction == Wall::Type::VERTIC)
	{
		for (int i = 0; i < 5; ++i)
		{
			SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), COORD{ C.X, C.Y++ });
			os << "#" << std::endl;
		}
	}

	return os;
}

void Wall::afisareWall( std::pair<int, int> position)
{
	
	if (m_direction == Wall::Type::HORIZ)
	{
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), COORD{ static_cast<short>(position.first),static_cast<short> (position.second) });
		//SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), COORD{ static_cast<short>(position.first),static_cast<short> (position.second) });
		std::cout << "#########";
	}

	else if (m_direction == Wall::Type::VERTIC)
	{
		for (int i = 0; i < 5; ++i)
		{
			SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), COORD{ static_cast<short>(position.first),static_cast<short> (position.second++ )});
			std::cout << "#" << std::endl;
		}
	}

	
}
