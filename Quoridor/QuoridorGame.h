#pragma once

#include <iostream>
#include "Piece.h"
#include "Console.h"
#include "Player.h"
#include "BoardStateChecker.h"

class QuoridorGame
{public:
	enum class Turn {
		Player1,
		Player2
	};

public:
	void Run();
private:
	int WinVerify(Console &C, Player& Player1, Player &Player2);
	void PlayerAction(Console& C, const int& option, Player& activePlayer,Player& passivePlayer);
};

