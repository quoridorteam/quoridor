#pragma once
#include <iostream>
#include <string>
#include "windows.h"
#include "Console.h"

class Wall
{
public:
	enum class Type {
		HORIZ,
		VERTIC,
		NONE
	};

public:
	Wall();
	Wall(const Type& direction,const std::pair<int,int>position);
	Wall(const std::pair<int, int> position);
	Wall(const Wall&other);
	friend std::ostream& operator <<(std::ostream& os, const Wall& other);
	void afisareWall(std::pair<int, int>position);

	//Getters
	Type GetDirection()const;
	std::pair<int,int> GetCoord()const;
	//Setters
	void SetDirection(const Type& direction);
	void SetCoord(const std::pair<int, int> newCoord);

private:
	Type m_direction:2;
	std::pair<int, int> coord;
};

