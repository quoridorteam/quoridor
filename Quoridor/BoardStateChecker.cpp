#include "BoardStateChecker.h"





BoardStateChecker::State BoardStateChecker::Check(Board &B,const Player & player1,const Player& player2)
{
	if (player1.GetPiece().GetPosition().second == B.GetBoardHeight()) {
		return State::Win;
	}
	if (player2.GetPiece().GetPosition().second == 1) {
		return State::Lose;
	}
	return State::Draw;
}
