#include "QuoridorGame.h"
#include "BoardStateChecker.h"
#include "Player.h"
#include <iostream>
#include <string>
#include "Board.h"
#include "../QuoridorLogging/QuoridorDll.h"
std::vector<std::pair<int,int>> occupiedPosition;

void QuoridorGame::PlayerAction(Console& C, const int& option, Player& activePlayer, Player& passivePlayer)
{
	if (option == 'P')
	{
		while(true) {
			try {
				activePlayer.MovePiece(C, passivePlayer,occupiedPosition);
				break;
			}catch (const char* errorMessage)
			{
				std::cout << errorMessage << std::endl;
			}
		}
	}
	else if (option == 'W')
	{
		if (activePlayer.GetToPlaceWalls() == 0)
			throw("Nu mai aveti ziduri disponibile.Acum puteti doar sa mutati piesa.");
		else
		{
			while (true) {
				try {
					std::cout << "Introduceti tipul de zid(V/O, A-H, 1-9)" << std::endl;
					char tip;
					char columnPosition;
					int linePosition;
					std::cin >> tip >> columnPosition >> linePosition;
					std::pair<int, int>wallPosition;
					Wall::Type wallType;
					if (tip == 'V') {
						wallPosition = activePlayer.VerticalWallPosition(C, columnPosition, linePosition);
						wallType = Wall::Type::VERTIC;
					}
					else if (tip == 'O') {
						wallPosition = activePlayer.OrizontalWallPosition(C, columnPosition, linePosition);
						wallType = Wall::Type::HORIZ;
					}

					Wall wall(wallType, wallPosition);

					/*if (player.verifyWall(wall) == true) */

					if (activePlayer.verifyWall(occupiedPosition, wall)) {
						if (tip == 'V' && (columnPosition >= 'A'&&columnPosition <= 'H')
							&& (linePosition >= 1 && linePosition <= 9))
						{

							activePlayer.PlaceWall(C, Wall::Type::VERTIC, wallPosition);
							occupiedPosition.push_back(wallPosition);
						}

						else if (tip == 'O' && (columnPosition >= 'A'&&columnPosition <= 'H')
							&& (linePosition >= 1 && linePosition <= 9))
						{

							activePlayer.PlaceWall(C, Wall::Type::HORIZ, wallPosition);
							occupiedPosition.push_back(wallPosition);
						}

					}
					break;
				}
				catch (const char* errorMessage) {

					std::cout << errorMessage << std::endl;
				}
			}

			}
		}
		
	
	
	//else if(option!='P'&&option!='W')
	//{
	//	throw("Introduceti un caracter valid:P pentru a muta player-ul si W pentru a plasa zid");
	//}

}

int QuoridorGame::WinVerify(Console &C, Player& Player1 ,Player &Player2)
{
	Board board;
	/*if (Player1.m_piece.getPosition().first*C.kHorizDistance == C.kBoardWidth)
	{
		std::cout << std::endl << "Player 1 wins";

		return 1;
	}
	else if (Player2.m_piece.getPosition().second*C.kVerticDistance == 3)
	{
		std::cout << std::endl << "Player 2 wins";
		return 1;
	}
	return 0;
	*/
	auto checkResult = BoardStateChecker::Check(board, Player1,Player2);
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	if (checkResult == BoardStateChecker::State::Win)
	{
		std::cout << "Player1 won" << std::endl;
		
		logger.log("Player1 Won", Logger::Level::Info);

		return 1;
	}
	else if (checkResult == BoardStateChecker::State::Lose) {
		std::cout << "Player2 won" << std::endl;
		logger.log("Player2 Won", Logger::Level::Info);
		return 1;
	}
	else{
		
		return 0;
	}
	
	
}

void InitializePlayer(Player && player, Console&& C, int&& tablePos, uint8_t&& color)
{	
	uint16_t startPos;
	std::string name;
	C.PlaceCursorToDef();
	std::cout << "Choose name for Player: ";
	std::cin >> name;
	C.PlaceCursorToDef();
	std::cout << "Choose start position for Player: ";
	std::cin >> startPos;
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.Verificare(startPos);
	if (startPos > 9)
		throw("Choose a valid start position");
	

	Player AuxPlayer(Piece('1', std::pair(startPos, tablePos), color), name);
	player = AuxPlayer;
	const auto&[coordI, coordJ] = player.GetPiece().GetPosition();
	C.CursorMove(coordI, coordJ);
	C.PrintColouredText(player.GetPiece(), player.GetPiece().GetColor());

}

void QuoridorGame::Run()
{
	
	Console C(std::pair<uint8_t, uint8_t>(4, 30));

	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log("Jocul a inceput", Logger::Level::Info);
	Board board;

	board.PrintBoard();

	
	
	logger.log("Tabla de joc creata si afisata", Logger::Level::Info);

	Player Player1;
	while (true) {
		try {
			InitializePlayer(std::move(Player1), std::move(C), 1, std::move(C.GetRed()));
			
			logger.log("primul jucator este creat", Logger::Level::Info);
			break;
		}
		catch (const char* errorMessage)
		{
			std::cout << errorMessage << std::endl;
		}
	}
	Player Player2;
	while (true) {
		try
		{
			InitializePlayer(std::move(Player2), std::move(C), 9, std::move(C.GetGreen()));
			
			logger.log("Al doilea jucator este creat", Logger::Level::Info);
			break;

		}
		catch (const char* errorMessage)
		{
			std::cout << errorMessage << std::endl;
		}
	}

	Turn turn = Turn::Player1;
	std::reference_wrapper<Player> currentPlayer = Player1;
	std::reference_wrapper<Player> passivePlayer = Player2;

	bool game_running = true;
	char option;

	while (game_running)
	{
		C.PlaceCursorToDef();
		if (turn == Turn::Player1)
		{
			std::cout << "Player 1 has a turn. " << "Choose option:  " << '\b';
			
			while (true) {
				std::cin >> option;
				std::ofstream of("syslog.log", std::ios::app);
				Logger logger(of);
				//logger.VerificareOptiune(option);
				try {
					logger.VerificareOptiune(option);
					PlayerAction(C, option, Player1,Player2);
					
					break;
				}
			
			catch (const char* errorMessage)
			{
				std::cout << errorMessage << std::endl;
			}
			}
			turn = Turn::Player2;
		}
		else
		{
			std::cout << "Player 2 has a turn. " << "Choose option:  " << '\b';
			
			while (true) {
				try {std::cin >> option;
					PlayerAction(C, option, Player2,Player1);
					break;
				}
				catch (const char* errorMessage)
				{
					std::cout << errorMessage << std::endl;
				}
			}
			turn = Turn::Player1;
		}
		if (WinVerify(C, Player1, Player2) == 1) {
			WinVerify(C, Player1, Player2);
			game_running = false;
		}

		std::swap(currentPlayer, passivePlayer);

	}
	return;
}


